#!/usr/bin/fish
for m in (xrandr -display :0.0 --query | grep " connected" | cut -d" " -f1)
  #MONITOR=$m polybar --reload example &
  switch $m
  	case HDMI-1
		polybar --reload DeskPi-1 &
  	case HDMI-2
		polybar --reload DeskPi-2 &
	case eDP-1
		polybar --reload Laptop &
	end
end    
