# import aliases
if test ~/.aliases
    . ~/.aliases
end

# import profile
#if test ~/.profile
#    . ~/.profile
#end

#Add paths
set -x PATH $PATH /usr/sbin ~/.emacs.d/bin /usr/local/go/bin ~/.local/share/applications /var/lib/flatpak/exports/bin
set -x GOPATH $HOME/go

set BLUESKY_USERNAME mikezimmermann5.bsky.social
set BLUESKY_PASSWORD b4ty-2u2n-xusy-2u7o

set EDITOR "vim"  

set -g theme_display_git yes
set -g theme_display_git_dirty yes
set -g theme_display_git_untracked yes
set -g theme_display_git_ahead_verbose yes
set -g theme_display_git_dirty_verbose yes
set -g theme_display_git_stashed_verbose yes
set -g theme_display_git_default_branch yes
set -g theme_git_default_branches master main
set -g theme_git_worktree_support no 
set -g theme_use_abbreviated_branch_name yes 
set -g theme_display_vagrant yes
set -g theme_display_docker_machine no
set -g theme_display_k8s_context yes
set -g theme_display_hg yes
set -g theme_display_virtualenv no
set -g theme_display_nix no
set -g theme_display_ruby no
set -g theme_display_node no
set -g theme_display_user yes
set -g theme_display_hostname yes
set -g theme_display_vi no
set -g theme_display_date no
set -g theme_display_cmd_duration yes
set -g theme_title_display_process yes
set -g theme_title_display_pathi yes
set -g theme_title_display_user yes
set -g theme_title_use_abbreviated_path no
set -g theme_date_format "+%a %H:%M"
set -g theme_date_timezone Europe/Berlin
set -g theme_avoid_ambiguous_glyphs no
set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_show_exit_status yes
set -g theme_display_jobs_verbose yes
set -g default_user your_normal_user
set -g theme_color_scheme nord
set -g fish_prompt_pwd_dir_length 0
set -g theme_project_dir_length 1
set -g theme_newline_cursor no
set -g theme_newline_prompt '$ '

export BAT_THEME="Nord"

# Setup fish greeting
function fish_greeting
    clear
    #fortune | cowsay -f (ls -1 /usr/share/cowsay/cows/ | shuf -n1) | lolcat

    colorscript random
end
function bind_bang
    switch (commandline -t)[-1]
        case "!"
            commandline -t $history[1]; commandline -f repaint
        case "*"
            commandline -i !
    end
end

function bind_dollar
    switch (commandline -t)[-1]
        case "!"
            commandline -t ""
            commandline -f history-token-search-backward
        case "*"
            commandline -i '$'
    end
end

function fish_user_key_bindings
    bind ! bind_bang
    bind '$' bind_dollar
end

# Add this to you config.fish or equivalent.
# Fish don't support recursive calls so use f function
function f
    fff $argv
    set -q XDG_CACHE_HOME; or set XDG_CACHE_HOME $HOME/.cache
    cd (cat $XDG_CACHE_HOME/fff/.fff_d)
end

function countdown
    set start (math (date +%s) + $argv[1])
    while test $start -ge (date +%s)
        set time (math $start - (date +%s))
        printf '%s\r' (date -u -d "@$time" +%H:%M:%S)
        sleep 0.1
    end
end

function stopwatch
    set start (date +%s)
    while true
        set time (math (date +%s) - $start)
        printf '%s\r' (date -u -d "@$time" +%H:%M:%S)
        sleep 0.1
    end
end

